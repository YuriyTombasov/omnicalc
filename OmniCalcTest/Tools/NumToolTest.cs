﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static OmniCalc.Tools.NumTool;

namespace OmniCalcTest.Tools
{
    [TestClass]
    public class NumToolTest
    {
        [TestMethod]
        public void GivenAB_WhenSum_ThenReturnSumAB()
        {
            int a = 2;
            int b = -3;
            int expectedSum = a + b;
            int factSum = Sum(a, b);
            Assert.IsTrue(factSum == expectedSum);
        }

        [TestMethod]
        public void GivenAB_WhenSum_ThenReturnSumAB()
        {
            Assert.IsTrue(Sum(2, -3) == -1);
        }

    }
}
