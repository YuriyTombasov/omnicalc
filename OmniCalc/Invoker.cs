﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OmniCalc
{
    class Invoker
    {
        private List<Type> classes = new List<Type>();

        private Dictionary<string, MethodInfo> classAndMethodRelation = new Dictionary<string, MethodInfo>();

        public Invoker(params string[] classFullNames)
        {
            try
            {
                foreach (string className in classFullNames)
                {
                    classes.Add(Type.GetType(className));
                }

                foreach (Type cls in classes)
                {
                    MethodInfo m = cls.GetMethod("getAvailableOper");
                    List<MethodInfo> classMethods = (List<MethodInfo>) m.Invoke(null, null);
                    foreach (MethodInfo method in classMethods)
                    {
                        string methodWhithClass = String.Format($"{cls.Name}>{method.Name}");
                        classAndMethodRelation.Add(methodWhithClass, method);
                    }
                }
            } catch(Exception e)
            {
                Console.WriteLine("Ошибка инициализации Invoker: " + e.Message);
            }
        }

        public Dictionary<string, MethodInfo> GetRelations()
        {
            return classAndMethodRelation;
        }

        public List<string> GetExistingRelationsClassMethod(string metodName)
        {
            List<string> existingRelationClassMethod = new List<string>();
            foreach (string relation in classAndMethodRelation.Keys)
            {
                if (relation.Split('>')[1].Equals(metodName))
                {
                    existingRelationClassMethod.Add(relation);
                }
            }
            return existingRelationClassMethod;
        }

        public string GetAvailableOperations()
        {
            string result = "";
            foreach (MethodInfo mi in classAndMethodRelation.Values)
            {
                result += mi.Name + "\n";
            }
            return result;
        }

        public object Invoke(params string[] methodWhithParameters)
        {
            if (methodWhithParameters == null || methodWhithParameters.Length < 1) throw new Exception("invoke -> input parameter is null or void");

            object result = "что-то пошло не так, не выполнилась ни одна ветка метода invoke()";
            
            string metodName = methodWhithParameters[0];
            
            List <string> strParameters = new List<string>();
            for(int i = 1; i < methodWhithParameters.Length; i++)
            {
                strParameters.Add(methodWhithParameters[i]);
            }

            List<string> existingRelationClassMethod = GetExistingRelationsClassMethod(metodName);

            if (existingRelationClassMethod.Count == 0)
            {
                result = "Опреация не найдена. Список доступных операций:\n" + GetAvailableOperations();
            }
            else if(existingRelationClassMethod.Count == 1)
            {
                MethodInfo m;
                classAndMethodRelation.TryGetValue(existingRelationClassMethod[0], out m);
                ParameterInfo[] methodParameters = m.GetParameters();

                if(strParameters.Count == methodParameters.Length)
                {
                    object[] realParameters = new object[methodParameters.Length];
                    for(int i = 0; i < strParameters.Count; i++)
                    {
                        realParameters[i] = Convert.ChangeType(strParameters[i], methodParameters[i].ParameterType);
                    }
                    result = m.Invoke(null, realParameters);
                } else
                {
                    result = "Недопустимое кол-во параметров для этой операции.\n";
                    result += "Описание операции:\n";
                    result += m.ToString();
                }
            }
            return result;
        }




    }
}
