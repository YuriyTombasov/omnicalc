﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OmniCalc.Tools
{
    public class NumTool
    {
        private static List<MethodInfo> availableOper = null;

        public static List<MethodInfo> getAvailableOper()
        {
            if (availableOper != null) return availableOper;

            availableOper = new List<MethodInfo>();
            foreach (MethodInfo mInf in typeof(NumTool).GetMethods())
            {
                if (!mInf.DeclaringType.Name.Equals("Object") && !mInf.Name.Equals("getAvailableOper"))
                {
                    availableOper.Add(mInf);
                }
            }
            return availableOper;
        }

        public static int Sum(int a, int b)
        {
            int res = a + b;
            return res;
        }

        public static int Mult(int a, int b)
        {
            int res = a * b;
            return res;
        }

        public static double Div(int a, int b) 
        {
            if (b < 0) throw new DivideByZeroException("На ноль делить нельзя");

            double res;
            res = (double) a / b;
            return res;
        }

        public static int Pow(int powBase, int power)
        {
            if (powBase <= 0 || power <= 0) throw new Exception("Основание и степень должны быть положительными целыми числами");

            int res = 1;
            for (int i = 0; i < power; i++)
            {
                res *= powBase;
            }
            return res;
        }

        public static int Fact(int a)
        {
            if (a < 0) throw new Exception("Факториал существует только для неотрицательных целых чисел.");

            if (a == 0 || a == 1)
            {
                return 1;
            }
            else
            {
                return a * Fact(a - 1);
            }
            //return 0;
        }





    }
}
