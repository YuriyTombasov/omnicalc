﻿using System;
using System.Collections.Generic;
//using System.Reflection;
//using static OmniCalc.Tools.NumTool;

namespace OmniCalc
{
    class Calculator
    {
        static void Main(string[] args)
        {
            Invoker inv = new Invoker("OmniCalc.Tools.NumTool", "OmniCalc.Tools.StringTool");

            if (args.Length > 0)
            {
                object res;
                try
                {
                    res = inv.Invoke(args);
                }
                catch(Exception e)
                {
                    res = String.Format($"Запуск программы с аргументами командной строки завершился ошибкой. Ошибка вызова invoke(args)\n ==> {e.Message}");
                }
                Console.WriteLine(res);
                Console.ReadKey();
            }
            else
            {
                object res;
                const string exitKeyword = "exit";

                Console.WriteLine("Введите название операции");
                string userInput = Console.ReadLine();
                while (!userInput.Equals(exitKeyword))
                {
                    if (inv.GetExistingRelationsClassMethod(userInput).Count > 0)
                    {
                        List<string> userCommand = new List<string>();
                        userCommand.Add(userInput);
                        Console.WriteLine("\nТеперь введите параметры, каждый с новой строки. Пустая строка - завершение ввода");
                        string parameter = Console.ReadLine();
                        while (!parameter.Equals(""))
                        {
                            userCommand.Add(parameter);
                            parameter = Console.ReadLine();
                        }
                        try
                        {
                            res = inv.Invoke(userCommand.ToArray());
                        }
                        catch (Exception e)
                        {
                            res = String.Format($"Запуск программы с вводом данных в консоль завершился ошибкой. Ошибка вызова invoke(args)\n ==> {e.Message}");
                        }
                    }
                    else
                    {
                        res = "Опреация не найдена. Список доступных операций:\n" + inv.GetAvailableOperations();
                    }
                    Console.WriteLine(res);
                    Console.WriteLine("\nВведите название операции");
                    userInput = Console.ReadLine();
                }
            }
        }
    }
}
